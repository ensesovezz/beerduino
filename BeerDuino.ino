//DFRobot.com
//Compatible with the Arduino IDE 1.0
//Library version:1.1
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27,16,2);  // set the LCD address to 0x27 for a 16 chars and 2 line display

const int boton = 12; //the number of the key pin
const int tiempoAntirebote = 10;

int estadoBoton = 0;
int estadoBotonAnterior = 1;
int mostrar = 0;

unsigned long tiempo = 0;
unsigned long t_actualizado = 0;
unsigned long t_actualizado_2 = 0;

// Configuramos los pines del sensor Trigger y Echo
const int PinTrig = 7;
const int PinEcho = 6;

// Constante velocidad sonido en cm/s
const float VelSon = 34000.0;
float distancia;

/*
 * Antirebote
 * -------------------------
 */
boolean antirebote(int pin) {
  int contador = 0;
  boolean estado;
  boolean estadoAnterior;

  do {
    estado = digitalRead(pin);
    if (estado =! estadoAnterior) {
      contador = 0;
      estadoAnterior = estado;
    } else {
      contador = contador + 1;
    }
    delay(1);
  } while(contador < tiempoAntirebote);

  return estado;
}

/*
 * Método que inicia la secuencia del Trigger para comenzar a medir
 * -------------------------
 */
void iniciarTrigger()
{
  // Ponemos el Triiger en estado bajo y esperamos 2 ms
  digitalWrite(PinTrig, LOW);
  delayMicroseconds(2);
  
  // Ponemos el pin Trigger a estado alto y esperamos 10 ms
  digitalWrite(PinTrig, HIGH);
  delayMicroseconds(10);
  
  // Comenzamos poniendo el pin Trigger en estado bajo
  digitalWrite(PinTrig, LOW);
}

/*
 * Setup
 * -------------------------
 */
void setup()
{
  lcd.init();                      // initialize the lcd 
 
  // Print a message to the LCD.
  lcd.backlight();

  lcd.print("Pres. el boton");
  lcd.setCursor(0, 1);
  lcd.print("para iniciar");

  pinMode(boton,INPUT); //initialize the key pin as input 

  Serial.begin(9600);

  // Ponemos el pin Trig en modo salida
  pinMode(PinTrig, OUTPUT);
  // Ponemos el pin Echo en modo entrada
  pinMode(PinEcho, INPUT);
}

/*
 * Loop
 * -------------------------
 */
void loop() {
  tiempo = millis();

  //
  // Boton
  //
  estadoBoton = digitalRead(boton);
  if ( tiempo > t_actualizado + 50) {
    if(estadoBoton != estadoBotonAnterior) {
      if(estadoBoton == HIGH) {
        Serial.println("on");
        mostrar ++;
        if(mostrar > 3) {
          mostrar = 1;
        }
      }
      t_actualizado = tiempo;
    } 
  } // delay
  estadoBotonAnterior = estadoBoton;


  //
  // Display
  //
  if ( tiempo > t_actualizado_2 + 1000) {
    if(mostrar == 1) {
      lcd.clear();
      lcd.print("Temp MACERADO: ");
      lcd.setCursor(0, 1);
      lcd.print("46 C");
    } else if (mostrar == 2) {
      lcd.clear();
      lcd.print("Temp LICOR: ");
      lcd.setCursor(0, 1);
      lcd.print("77 C");  
    } else if (mostrar == 3) {
      lcd.clear();
      lcd.print("Litros para 67C ");
      lcd.setCursor(0, 1);
      lcd.print(distancia, 0);
      lcd.print(" L"); 
    }
    t_actualizado_2 = tiempo;
  }

  //
  // Distancia 
  //
  iniciarTrigger();
  // La función pulseIn obtiene el tiempo que tarda en cambiar entre estados, en este caso a HIGH
  unsigned long tiempo = pulseIn(PinEcho, HIGH);
  // Obtenemos la distancia en cm, hay que convertir el tiempo en segudos ya que está en microsegundos
  // por eso se multiplica por 0.000001
  distancia = tiempo * 0.000001 * VelSon / 2.0;
  distancia = distancia;

}
